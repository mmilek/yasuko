# Yasuko - the Discord bot for posting images from reddit
***
Yasuko is a simple Discord bot intended to post the most upvoted images from a subreddit to a specified channel. Currently, commands with a prefix don't exist. Everything is handled by the `on_message` function.
Written in Python, using the Discord API
#### Setup
You have to create a Discord app, using their Developer Portal. You can get your token and the invite link from there. When you're ready:
1. Clone the repo
2. Install the needed python modules
3. Replace the placeholder variables with your own (token, channel IDs, etc.)
4. Run
#### Functionality
1. Post the most upvoted image along with a random text message
2. After being asked to post more - decide whether to post or not based on the inclusion of "please" by the user
3. Insult or compliment a Polish football club
4. Replying to images posted a channel, adding randomized reactions to them 
5. FUTURE - Greet new users with a specified message
### License
This project is licensed under the terms of the *GNU GPLv2* license.
