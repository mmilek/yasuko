import discord
import wget
import requests
import os
import re
import random
from urllib.request import urlopen
from discord.ext import tasks
from datetime import datetime
from collections import OrderedDict

activity = discord.Activity(name='anime', type=discord.ActivityType.watching)
bot = discord.Client(activity=activity)
GIRL_TIME = "20:00" # the time at which Yasuko posts images
CHANNEL_ID = 0 # the channel where Yasuko will post images
TERMINAL_CHANNEL = 1 # your terminal channel. You can type messages there and Yasuko will post them in the main channel

all_links = []
number_posted_today = 0
can_send_more_today = True
posted_standard = False

messages = ["Hey! Here are today's top girls! (-‿◦)",
            "Hello, the time has come to post some cute girls!",
            f"Ohayo! It's {GIRL_TIME}, you know what that means ;)",
            "Moshi-moshi! Yes, this is Yasuko... yes, I'm posting!",
            "Hi!, I hope your day was nice so far!",
            "Phew, it wasn't easy to find them!",
            "Look! Here they are!",
            "You coomers! I know you've been waiting!",
            "Stop drooling! I'm posting, alright?",
            "I-It's not like I wanted to send you anything t-today...",
            "I just happened to find them... Don't get the wrong idea!",
            "The second one is kinda pretty, huh?",
            "Don't let your girlfriend see that... Wait, you do have a girlfriend, right?",
            "Are they better than real girls? Please tell me! Or maybe don't? I don't think I really want to know after all.",
            "3D vs 2D? Let me know ;)",
            "Here's what you've all been waiting for!",
            "Your desire for 2D girls is frightening...",
            "Here's a special present for you! Just kidding, I do this everyday~",
            "I like y'all so much! I'll keep posting~!",
            "They are getting prettier and prettier. I might start getting jealous!",
            "I'm posting. I have assessed the situation and I'm posting.",
            "You definitely should take them to Dorsia!",
            "( ͡° ͜ʖ ͡°)",
            "Enjoy, gentlemen!",
            "These are free and open source anime girls!"]

rejection_messages = [
            "Nah, I don't feel like it, just wait until tomorrow!",
            "I know you can be more polite than that!",
            "Try again tomorrow, with the magic word.",
            "I really can't see you getting laid with these kinds of manners...",
            "In your dreams!",
            "Are you retarded?",
            "╭∩╮༼ ◕_◕༽╭∩╮"]

polite_rejection_messages = [
            "I appreciate the politeness, but I just don't have any cute ones left!",
            "I'm sowwy, no more in stock ;c",
            "Sorry, I promise I'll get the cutest one tomorrow!",
            "Don't be too greedy!. I'll get more tomorrow ;3",]

approval_messages = ["Since you're asking so kindly....",
            "Awww! Of course. Here you are.",
            "Here it is, a picture just for my gentleman.",
            "Sure, here you go!",
            "Yeah, I guess I can post more ｡^‿^｡",
            "Seems like you just can't wait...",
            "A true fan, huh? (-‿◦)",
            "Yes, I can do that. (─‿─)",
            "Weren't the first three enough? But sure, I will get more!",
            "Is that enough?",
            "I know they're cute, no problem!"]

no_more_posts_messages = ["That's enough for today!",
                          "I've already sent you enough!",
                          "Do you really feel like it's okay to beg like that?",
                          "Aren't you pushing it a bit?",
                          "Chill, I'll send more tomorrow!",
                          "How many times are you going to try?",
                          "Calm down!",
                          "I don't have any pictures left...",
                          "No.",
                          "No, go get some maidens on your stick. (；⌣̀_⌣́)"]

arka_gdynia_messages = ["Kurwa świnia", "Dobra drużyna"]

emojis = ["❤️","💕","💗","💖","😍","🥵","🫀","💞"]

image_replies = ["Nice one!",
                 "Cute",
                 "Impressive. Very nice.",
                 "https://tenor.com/view/american-psycho-patrick-bateman-american-psycho-gif-7212093",
                 "https://tenor.com/view/impressive-very-nice-patrick-bateman-gif-15247511",
                 "Source? Asking for a friend.",
                 "Kinda hot tbh",
                 "Cuuuute!",
                 "Pretty nice, get us some more!",
                 "Awwwwww <3",
                 "UwU",
                 "Disturbingly astonishing.",
                 "Nice, I like her eyes",
                 "OwO",
                 "Oh, I know the anime she's from!"]

@bot.event
async def on_ready():
    print("Yasuko is now ready")
    daily_post.start()

async def download_image(link):
    global number_posted_today
    extension = link[-3::]
    code = link.split('/')[3].split('.')[0]
    wget.download(link); print('\n')

    # image compression
    if extension == 'jpg':
        os.system(f"jpegoptim --size=8000k {code}.{extension}")
    elif extension == 'png':
        os.system(f"optipng {code}.{extension}")

    img = discord.File(f'{code}.{extension}')
    return img

@bot.event
async def on_message(message):

    global number_posted_today
    global can_send_more_today
    global posted_standard
    global all_links

    time = str(datetime.now())[11:16]

    if time >= GIRL_TIME and posted_standard == True:
        if message.channel.id == CHANNEL_ID and message.author.name != "Yasuko" and len(message.attachments)==0:
            if number_posted_today < 5 and can_send_more_today == True:
                rand = random.randrange(0,10)
                if "more" in message.content.lower() and "please" not in message.content.lower():
                    if rand < 3:
                        message_text = random.choice(approval_messages)
                        image = await download_image(all_links[number_posted_today])
                        await message.channel.send(f"{message_text}", file=image)
                        number_posted_today += 1
                        os.system(f"rm {image.filename}")
                    else:
                        await message.channel.send(f"{random.choice(rejection_messages)}")
                        can_send_more_today = False
                        print(can_send_more_today)

                elif "more" in message.content.lower() and "please" in message.content.lower():
                    if rand < 9:
                        message_text = random.choice(approval_messages)
                        image = await download_image(all_links[number_posted_today])
                        await message.channel.send(f"{message_text}", file=image)
                        number_posted_today += 1
                        os.system(f"rm {image.filename}")

                    else:
                        await message.channel.send(f"{random.choice(polite_rejection_messages)}")
                        can_send_more_today = False
            elif "more" in message.content.lower():
                await message.channel.send(f"{random.choice(no_more_posts_messages)}")

    if len(message.attachments) > 0 and message.channel.id == CHANNEL_ID and message.author.name != "Yasuko":
        check = all(attachment.content_type == "image/jpeg" or attachment.content_type == "image/png" for attachment in message.attachments)
        if check:
            rand = random.randrange(0,10)
            if rand < 5:
                await message.add_reaction(f"{random.choice(emojis)}")
                await message.reply(content=f"{random.choice(image_replies)}")

    if message.content.lower() == "arka gdynia":
        await message.channel.send(f"{random.choice(arka_gdynia_messages)}")

    if message.channel.id == TERMINAL_CHANNEL and message.author.name != 'Yasuko':
        girls_channel =  bot.get_channel(CHANNEL_ID)
        await girls_channel.send(f"{message.content}")
        await message.channel.send("Message has been forwarded.")


@tasks.loop(seconds=60)
async def daily_post():

    global number_posted_today
    global all_links
    global can_send_more_today
    global posted_standard

    time = str(datetime.now())[11:16]
    print(time)
    posted_standard = True

    if time == GIRL_TIME:
        posted_standard = False
        all_links = []
        # zero the number of posts today (after GIRL_HOUR)
        number_posted_today = 0
        girls_channel = bot.get_channel(CHANNEL_ID)
        # set this so that the bot can be asked for more later
        can_send_more_today = True

        while(len(all_links) == 0):

            #get the subreddit page
            page = urlopen('https://www.reddit.com/r/awwnime/top/')
            page = page.read().decode('utf8')

            #find valid links
            x = re.findall(r"https:\/\/i\.imgur\.com\/[a-zA-Z0-9]+\.jpg|https:\/\/preview\.redd\.it\/[a-zA-Z0-9]+\.jpg|https:\/\/i\.imgur\.com\/[a-zA-Z0-9]+\.png|https:\/\/preview\.redd\.it\/[a-zA-Z0-9]+\.png", page)
            link_list = list(OrderedDict.fromkeys(x))

            for link in link_list:
                if "https://preview.redd.it/" in link:
                    extension = link.split('/')[3].split('.')[1].split('?')[0]
                    code = link.split('/')[3].split('.')[0]
                    link = f"https://i.redd.it/{code}.{extension}"
                    all_links.append(link)
                else:
                    all_links.append(link)
            print("LIST OF LINKS:")
            print(all_links)

        while number_posted_today < 3:
            link = all_links[number_posted_today]
            if number_posted_today == 0:
                message = random.choice(messages)
                image = await download_image(all_links[number_posted_today])
                await girls_channel.send(f"{message}", file=image)
                os.system(f"rm {image.filename}")
            else:
                image = await download_image(all_links[number_posted_today])
                await girls_channel.send(f"", file=image)
                os.system(f"rm {image.filename}")
            number_posted_today += 1
        posted_standard = True

bot.run("YOUR_TOKEN")
